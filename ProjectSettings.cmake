## ----------------------------------------------------
## Please see Documentation/quasarBuildSystem.html for
## information how to use this file.
## ----------------------------------------------------

#set(SERVERCONFIG_LOADER TRUE) # not available in open6 yet

if (EXISTS ${PROJECT_SOURCE_DIR}/QuasarModuleITkStripsDCS)
  message("WITH QUASAR MODULE: " ${PROJECT_SOURCE_DIR}/QuasarModuleITkStripsDCS)

  set(BUILD_WITH_QuasarModuleITkStripsDCS TRUE)

  #set(MODULE_BACKEND "comline_endeavourTest")
  #set(MODULE_BACKEND "comline_powertools")
  #set(MODULE_BACKEND "ICNETIO")
  set(BUILD_endeavourTest TRUE)
  set(BUILD_ICNETIO FALSE)
  set(BUILD_FELIXCLIENT TRUE)

  set(IGNORE_DEFAULT_BOOST_SETUP OFF)

  # module "backend" is the HW connection to AMAC
  # it is provided by some commandline utility, like endeavourTest from ITSDAQ
  # or, in Felix setup, by a netio library that connects to Felix

  if( "${MODULE_BACKEND}" STREQUAL "comline_powertools")
    add_compile_definitions(BUILD_WITH_QuasarModuleITkStripsDCS_comline_powertools)
  endif()

  if(${BUILD_ICNETIO}) # different netio libraries
    add_compile_definitions(BUILD_WITH_QuasarModuleITkStripsDCS)
    add_compile_definitions(BUILD_WITH_ICNETIO)  # <------------------- to get the native netio for IC lpGBT link!

    if( "${MODULE_BACKEND}" STREQUAL "YARR")
    ## or you use YARR as the netio backend (it links netio statically inside itself?)
    ##export YARR_DIR=/home/itkfelixstrips/tests/YARR/
    ##cmake3 -D YARR_DIR:PATH=${YARR_DIR} -S . -B build/ -DCMAKE_BUILD_TYPE=Debug
    set(YARR_DIR "/home/itkfelixstrips/tests/YARR/")   # <------------------------------ YARR option

    elseif( ${BUILD_ICNETIO})
    ## the build with ic_comm as netio backend
    set(ICNETIO        true)     # <------------------------------------------ ICNETIO option
    set(CHECKOUT_NETIO true)
    message("ICNETIO: " ${ICNETIO})
    message("CHECKOUT_NETIO: " ${CHECKOUT_NETIO})

    elseif( "${MODULE_BACKEND}" STREQUAL "ICNETIO_FELIX")
    ##export FELIX_ROOT=/home/itkfelixstrips/FELIXSw/
    ##cmake3 -S . -B build/ -DCMAKE_BUILD_TYPE=Debug -D ICNETIO:bool=true -D SOFTWARE_PATH:PATH=${FELIX_ROOT}
    set(SOFTWARE_PATH "/home/itkfelixstrips/FELIXSw/")
    #message("SOFTWARE_PATH: " ${SOFTWARE_PATH})
    ## it also needs export FELIX_ROOT=/home/itkfelixstrips/FELIXSw/
    #message("ENV FELIX_ROOT: " $ENV{FELIX_ROOT})
    endif()

    include_directories(./QuasarModuleITkStripsDCS/ic-over-netio/)
  endif()

  if( ${BUILD_endeavourTest})
    add_compile_definitions(BUILD_WITH_QuasarModuleITkStripsDCS_comline_endeavourTest)
  endif()

  if(${BUILD_FELIXCLIENT})
    #set(CMAKE_INSTALL_PREFIX "/home/itkstrips/.local/opt/atlas/")
    set(CMAKE_INSTALL_PREFIX "${CMAKE_CURRENT_BINARY_DIR}/")
    set(ELINKCLIENT true)
    add_compile_definitions(BUILD_WITH_QuasarModuleITkStripsDCS)
    add_compile_definitions(BUILD_WITH_ELINKCLIENT)
    message("Quasar building FELIXCLIENT install in ${CMAKE_INSTALL_PREFIX}")
  endif()

  set(CUSTOM_SERVER_MODULES QuasarModuleITkStripsDCS)
  include_directories(./QuasarModuleITkStripsDCS/)
  include_directories(./QuasarModuleITkStripsDCS/powertools/pbv3/)

  include_directories(./QuasarModuleITkStripsDCS/powertools/labremote/src/libDevCom/)
  include_directories(./QuasarModuleITkStripsDCS/powertools/labremote/src/libUtils/)
  include_directories("./QuasarModuleITkStripsDCS/powertools/labremote/src/exts/nlohmann_json/include/")
  #include_directories(./QuasarModuleITkStripsDCS/powertools/labremote/src/libEquipConf/)
  #include_directories(./QuasarModuleITkStripsDCS/powertools/labremote/src/libPS/)
  #include_directories(./QuasarModuleITkStripsDCS/powertools/labremote/src/libCom/)

  # move it under FELIXCLIENT if branch?
  include_directories("${CMAKE_CURRENT_BINARY_DIR}/_deps/spdlog-src/include/")
  include_directories("${CMAKE_CURRENT_BINARY_DIR}/_deps/variant-src/include/")
else()
  message("NO QUASAR MODULE: " ${PROJECT_SOURCE_DIR}/QuasarModuleITkStripsDCS)
endif()


set(EXECUTABLE OpcUaServer)
set(SERVER_INCLUDE_DIRECTORIES  )
set(SERVER_LINK_LIBRARIES  )
set(SERVER_LINK_DIRECTORIES  )

##
## If ON, in addition to an executable, a shared object will be created.
##
set(BUILD_SERVER_SHARED_LIB OFF)

##
## Add here any additional boost libraries needed with their canonical name
## examples: date_time atomic etc.
## Note: boost paths are resolved either from $BOOST_ROOT if defined or system paths as fallback
##
set(ADDITIONAL_BOOST_LIBS )
