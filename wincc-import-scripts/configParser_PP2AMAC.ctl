
// generated using Cacophony, an optional module of quasar, see: https://github.com/quasar-team/Cacophony



const string CONNECTIONSETTING_KEY_DRIVER_NUMBER = "DRIVER_NUMBER";
const string CONNECTIONSETTING_KEY_SERVER_NAME = "SERVER_NAME";
const string CONNECTIONSETTING_KEY_SUBSCRIPTION_NAME = "SUBSCRIPTION_NAME";

bool pp2AMACdpTypeExists(string dpt)
{
    dyn_string queriedTypes = dpTypes(dpt);
    return (dynlen(queriedTypes) >= 1);
}

bool pp2AMACaddressConfigWrapper (
    string  dpe,
    string  address,
    int     mode,
    mapping connectionSettings,
    bool active=true
)
{
    string subscription = "";
    if (mode != DPATTR_ADDR_MODE_IO_SQUERY && mode != DPATTR_ADDR_MODE_INPUT_SQUERY)
    {
        subscription = connectionSettings[CONNECTIONSETTING_KEY_SUBSCRIPTION_NAME];
    }
    dyn_string dsExceptionInfo;
    fwPeriphAddress_setOPCUA (
        dpe /*dpe*/,
        connectionSettings[CONNECTIONSETTING_KEY_SERVER_NAME],
        connectionSettings[CONNECTIONSETTING_KEY_DRIVER_NUMBER],
        "ns=2;s="+address,
        subscription /* subscription*/,
        1 /* kind */,
        1 /* variant */,
        750 /* datatype */,
        mode,
        "" /*poll group */,
        dsExceptionInfo
    );
    if (dynlen(dsExceptionInfo)>0)
        return false;
    //DebugTN("Setting active on dpe: "+dpe+" to "+active);
    dpSetWait(dpe + ":_address.._active", active);

    return true;
}

bool pp2AMACevaluateActive(
    mapping addressActiveControl,
    string className,
    string varName,
    string dpe)
{
    bool active = false;
    if (mappingHasKey(addressActiveControl, className))
    {
        string regex = addressActiveControl[className];
        int regexMatchResult = regexpIndex(regex, varName, makeMapping("caseSensitive", true));
        DebugTN("The result of evaluating regex: '"+regex+"' with string: '"+varName+" was: "+regexMatchResult);
        if (regexMatchResult>=0)
            active = true;
        else
        {
            active = false;
            DebugN("Note: the address on dpe: "+dpe+" will be non-active because such instructions were passed in the addressActive mapping.");
        }
    }
    else
        active = true; // by default
    return active;
}


bool pp2AMACconfigureDCDCen (
    int     docNum,
    int     childNode,
    string  prefix,
    bool    createDps,
    bool    assignAddresses,
    bool    continueOnError,
    mapping addressActiveControl,
    mapping connectionSettings)
{
    DebugN("Configure.DCDCen called");
    string name = "DCDC";
    //xmlGetElementAttribute(docNum, childNode, "name", name);
    string fullName = prefix+name;
    string dpt = "PP2AMACDCDCen";

    if (pp2AMACdpTypeExists(dpt))
    {

        if (createDps)
        {

            DebugTN("Will create DP "+fullName);
            int result = dpCreate(fullName, dpt);
            if (result != 0)
            {
                DebugTN("dpCreate name='"+fullName+"' dpt='"+dpt+"' not successful or already existing");
                if (!continueOnError)
                    throw(makeError("Cacophony", PRIO_SEVERE, ERR_IMPL, 1, "XXX YYY ZZZ"));
            }
        }

        if (assignAddresses)
        {
            string dpe, address;
            dyn_string dsExceptionInfo;
            bool success;
            bool active = false;

            dpe = fullName+".DCDCen";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "DCDCen",
                         "DCDCen",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }


        }
    }

    dyn_int children;

}


bool pp2AMACconfigureHVcnt (
    int     docNum,
    int     childNode,
    string  prefix,
    bool    createDps,
    bool    assignAddresses,
    bool    continueOnError,
    mapping addressActiveControl,
    mapping connectionSettings)
{
    DebugN("Configure.HVcnt called");
    string name = "HV";
    //xmlGetElementAttribute(docNum, childNode, "name", name);
    string fullName = prefix+name;
    string dpt = "PP2AMACHVcnt";

    if (pp2AMACdpTypeExists(dpt))
    {

        if (createDps)
        {

            DebugTN("Will create DP "+fullName);
            int result = dpCreate(fullName, dpt);
            if (result != 0)
            {
                DebugTN("dpCreate name='"+fullName+"' dpt='"+dpt+"' not successful or already existing");
                if (!continueOnError)
                    throw(makeError("Cacophony", PRIO_SEVERE, ERR_IMPL, 1, "XXX YYY ZZZ"));
            }
        }

        if (assignAddresses)
        {
            string dpe, address;
            dyn_string dsExceptionInfo;
            bool success;
            bool active = false;

            dpe = fullName+".CntSetHV0en";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "HVcnt",
                         "CntSetHV0en",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".CntSetHV1en";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "HVcnt",
                         "CntSetHV1en",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".CntSetHV2en";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "HVcnt",
                         "CntSetHV2en",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".CntSetHV3en";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "HVcnt",
                         "CntSetHV3en",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".CntSetHV0Freq";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "HVcnt",
                         "CntSetHV0Freq",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".CntSetHV1Freq";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "HVcnt",
                         "CntSetHV1Freq",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".CntSetHV2Freq";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "HVcnt",
                         "CntSetHV2Freq",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".CntSetHV3Freq";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "HVcnt",
                         "CntSetHV3Freq",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }


        }
    }

    dyn_int children;

}


bool pp2AMACconfigureLDOcnt (
    int     docNum,
    int     childNode,
    string  prefix,
    bool    createDps,
    bool    assignAddresses,
    bool    continueOnError,
    mapping addressActiveControl,
    mapping connectionSettings)
{
    DebugTN("Configure.LDOcnt called");
    string name = "LDO";
    //xmlGetElementAttribute(docNum, childNode, "name", name);
    string fullName = prefix+name;
    string dpt = "PP2AMACLDOcnt";

    if (pp2AMACdpTypeExists(dpt))
    {

        if (createDps)
        {

            DebugTN("Will create DP "+fullName);
            int result = dpCreate(fullName, dpt);
            if (result != 0)
            {
                DebugTN("dpCreate name='"+fullName+"' dpt='"+dpt+"' not successful or already existing");
                if (!continueOnError)
                    throw(makeError("Cacophony", PRIO_SEVERE, ERR_IMPL, 1, "XXX YYY ZZZ"));
            }
        }

        if (assignAddresses)
        {
            string dpe, address;
            dyn_string dsExceptionInfo;
            bool success;
            bool active = false;

            dpe = fullName+".CntSetHyLDO0en";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "LDOcnt",
                         "CntSetHyLDO0en",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".CntSetHyLDO1en";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "LDOcnt",
                         "CntSetHyLDO1en",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".CntSetHyLDO2en";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "LDOcnt",
                         "CntSetHyLDO2en",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".CntSetHxLDO0en";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "LDOcnt",
                         "CntSetHxLDO0en",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".CntSetHxLDO1en";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "LDOcnt",
                         "CntSetHxLDO1en",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".CntSetHxLDO2en";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "LDOcnt",
                         "CntSetHxLDO2en",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }


        }
    }

    dyn_int children;

}


bool pp2AMACconfigureAMAC (
    int     docNum,
    int     childNode,
    string  prefix,
    bool    createDps,
    bool    assignAddresses,
    bool    continueOnError,
    mapping addressActiveControl,
    mapping connectionSettings)
{
    DebugTN("Configure.AMAC called");
    string name;
    xmlGetElementAttribute(docNum, childNode, "name", name);
    string fullName = prefix+name;
    string dpt = "PP2AMACAMAC";

    if (pp2AMACdpTypeExists(dpt))
    {

        if (createDps)
        {

            DebugTN("Will create DP "+fullName);
            int result = dpCreate(fullName, dpt);
            if (result != 0)
            {
                DebugTN("dpCreate name='"+fullName+"' dpt='"+dpt+"' not successful or already existing");
                if (!continueOnError)
                    throw(makeError("Cacophony", PRIO_SEVERE, ERR_IMPL, 1, "XXX YYY ZZZ"));
            }
        }

        if (assignAddresses)
        {
            string dpe, address;
            dyn_string dsExceptionInfo;
            bool success;
            bool active = false;

            dpe = fullName+".AMACID";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "AMACID",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".AMACVersion";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "AMACVersion",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Enabled";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Enabled",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".State";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "State",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Status";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Status",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".EnabledRegisters";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "EnabledRegisters",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".RegistersToEnable";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "RegistersToEnable",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".RegistersToDisable";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "RegistersToDisable",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".MonitorDCDC";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "MonitorDCDC",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".read";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "read",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".write";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "write",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".useFuseID";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "useFuseID",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".WriteConfig";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "WriteConfig",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".DCDCOn";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "DCDCOn",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".HVOn";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "HVOn",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".CntSetHV0en";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "CntSetHV0en",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".CntSetHV1en";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "CntSetHV1en",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".CntSetHV2en";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "CntSetHV2en",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".CntSetHV3en";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "CntSetHV3en",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".CntSetHV0Freq";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "CntSetHV0Freq",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".CntSetHV1Freq";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "CntSetHV1Freq",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".CntSetHV2Freq";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "CntSetHV2Freq",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".CntSetHV3Freq";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "CntSetHV3Freq",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".ShuntEn";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "ShuntEn",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".DACShunty";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "DACShunty",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".DACShuntx";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "DACShuntx",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".recalibrate";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "recalibrate",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".CalibrationFile";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "CalibrationFile",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".temperatureX";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "temperatureX",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".temperatureY";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "temperatureY",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".temperaturePB";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "temperaturePB",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".temperaturePTAT";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "temperaturePTAT",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".temperatureCTAT";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "temperatureCTAT",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".getCur10V";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "getCur10V",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".getCur1V";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "getCur1V",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".current10V";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "current10V",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".current1V";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "current1V",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".PP2_I_out";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "PP2_I_out",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".PP2_V_sense";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "PP2_V_sense",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".PP2_V_ret";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "PP2_V_ret",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".PP2_V_in";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "PP2_V_in",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".PP2_V_out";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "PP2_V_out",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".DCDCen";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "DCDCen",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".DCDCenC";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "DCDCenC",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".VDCDC";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "VDCDC",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".VDDLR";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "VDDLR",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".DCDCin";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "DCDCin",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".VDDREG";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "VDDREG",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".sysBG";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "sysBG",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".AM900BG";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "AM900BG",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".AM600BG";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "AM600BG",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".CAL";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "CAL",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".AMref";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "AMref",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".CALx";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "CALx",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".CALy";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "CALy",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Shuntx";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Shuntx",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Shunty";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Shunty",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".HGND";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "HGND",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".DIETEMP";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "DIETEMP",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".NTCx";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "NTCx",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".NTCy";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "NTCy",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".NTCpb";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "NTCpb",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".VoltNTCx";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "VoltNTCx",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".VoltNTCy";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "VoltNTCy",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".VoltNTCpb";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "VoltNTCpb",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Hrefx";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Hrefx",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Hrefy";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Hrefy",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Cur10V";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Cur10V",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Cur10VTPlow";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Cur10VTPlow",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Cur10VTPhigh";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Cur10VTPhigh",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Cur1V";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Cur1V",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Cur1VTPlow";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Cur1VTPlow",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Cur1VTPhigh";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Cur1VTPhigh",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".HVret";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "HVret",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".PTAT";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "PTAT",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg000";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg000",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg001";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg001",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg002";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg002",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg003";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg003",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg004";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg004",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg005";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg005",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg006";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg006",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg007";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg007",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg010";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg010",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg010_Ch0";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg010_Ch0",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg010_Ch1";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg010_Ch1",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg010_Ch2";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg010_Ch2",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg010_Ae";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg010_Ae",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg011";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg011",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg011_Ch3";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg011_Ch3",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg011_Ch4";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg011_Ch4",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg011_Ch5";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg011_Ch5",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg011_Ae";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg011_Ae",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg012";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg012",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg012_Ch6";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg012_Ch6",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg012_Ch7";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg012_Ch7",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg012_Ch8";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg012_Ch8",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg012_Ae";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg012_Ae",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg013";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg013",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg013_Ch09";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg013_Ch09",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg013_Ch10";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg013_Ch10",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg013_Ch11";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg013_Ch11",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg013_Ae";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg013_Ae",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg014";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg014",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg014_Ch12";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg014_Ch12",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg014_Ch13";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg014_Ch13",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg014_Ch14";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg014_Ch14",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg014_Ae";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg014_Ae",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg015";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg015",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg015_Ch15";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg015_Ch15",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg015_Ae";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg015_Ae",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg024";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg024",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg025";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg025",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg026";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg026",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg027";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg027",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg031";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg031",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg032";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg032",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg033";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg033",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg034";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg034",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg040_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg040_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg041_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg041_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg042_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg042_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg043_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg043_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg044_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg044_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg045_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg045_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg046_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg046_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg047_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg047_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg048_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg048_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg049_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg049_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg050_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg050_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg051_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg051_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg052_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg052_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg053_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg053_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg054_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg054_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg055_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg055_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg056_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg056_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg057_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg057_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg058_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg058_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg060_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg060_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg061_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg061_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg062_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg062_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg063_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg063_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg064_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg064_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg065_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg065_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg070_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg070_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg071_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg071_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg072_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg072_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg073_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg073_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg074_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg074_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg075_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg075_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg076_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg076_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg077_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg077_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg078_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg078_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg079_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg079_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg080_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg080_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg081_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg081_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg082_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg082_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg083_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg083_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg084_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg084_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg085_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg085_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg086_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg086_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg087_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg087_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg090_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg090_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg091_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg091_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg092_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg092_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg093_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg093_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg094_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg094_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg095_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg095_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg096_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg096_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg100_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg100_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg101_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg101_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg102_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg102_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg103_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg103_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg104_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg104_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg105_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg105_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg106_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg106_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg107_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg107_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg108_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg108_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg109_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg109_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg110_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg110_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg111_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg111_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg112_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg112_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg113_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg113_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg114_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg114_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg115_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg115_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg116_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg116_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg117_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg117_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg118_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg118_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg119_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg119_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg120_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg120_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg121_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg121_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg122_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg122_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg123_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg123_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg124_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg124_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg125_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg125_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg126_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg126_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg127_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg127_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg128_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg128_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg129_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg129_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg130_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg130_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg131_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg131_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg132_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg132_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg133_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg133_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg134_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg134_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg135_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg135_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg136_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg136_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg137_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg137_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg138_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg138_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg139_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg139_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg140_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg140_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg141_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg141_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg142_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg142_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg143_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg143_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg144_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg144_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg145_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg145_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg146_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg146_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg147_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg147_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg148_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg148_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg149_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg149_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg150_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg150_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg151_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg151_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg152_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg152_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg153_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg153_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg154_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg154_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg155_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg155_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg156_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg156_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg157_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg157_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg158_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg158_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg159_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg159_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg160_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg160_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg161_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg161_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg162_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg162_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg163_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg163_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg164_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg164_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg165_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg165_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg166_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg166_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg167_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg167_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg168_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg168_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg169_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg169_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg170_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg170_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reg171_RW";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "AMAC",
                         "Reg171_RW",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }


        }
    }

    dyn_int children;
    // by hand: add DCDCen HVcnt and LDOcnt
    // where pp2AMACgetChildNodesWithName gets the number of children?
    // it seems it gets them from the config.xml
    // int node = xmlFirstChild(docNum, parentNode);
    // let's hardcode children = 1
    DebugTN("Configure AMAC Design-initialized children");
    //children = 1;
    //for (int i=1; i<=dynlen(children); i++)
    pp2AMACconfigureDCDCen (docNum, 0, fullName+"/", createDps, assignAddresses, continueOnError, addressActiveControl, connectionSettings);

    // HVcnt
    //children = 1;
    //for (int i=1; i<=dynlen(children); i++)
    pp2AMACconfigureHVcnt (docNum, 0, fullName+"/", createDps, assignAddresses, continueOnError, addressActiveControl, connectionSettings);

    // LDOcnt
    //children = 1;
    //for (int i=1; i<=dynlen(children); i++)
    pp2AMACconfigureLDOcnt (docNum, 0, fullName+"/", createDps, assignAddresses, continueOnError, addressActiveControl, connectionSettings);
}


bool pp2AMACconfigureLpGBT (
    int     docNum,
    int     childNode,
    string  prefix,
    bool    createDps,
    bool    assignAddresses,
    bool    continueOnError,
    mapping addressActiveControl,
    mapping connectionSettings)
{
    DebugTN("Configure.LpGBT called");
    string name;
    xmlGetElementAttribute(docNum, childNode, "name", name);
    string fullName = prefix+name;
    string dpt = "PP2AMACLpGBT";

    if (pp2AMACdpTypeExists(dpt))
    {

        if (createDps)
        {

            DebugTN("Will create DP "+fullName);
            int result = dpCreate(fullName, dpt);
            if (result != 0)
            {
                DebugTN("dpCreate name='"+fullName+"' dpt='"+dpt+"' not successful or already existing");
                if (!continueOnError)
                    throw(makeError("Cacophony", PRIO_SEVERE, ERR_IMPL, 1, "XXX YYY ZZZ"));
            }
        }

        if (assignAddresses)
        {
            string dpe, address;
            dyn_string dsExceptionInfo;
            bool success;
            bool active = false;

            dpe = fullName+".hostname";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "LpGBT",
                         "hostname",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".portRX";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "LpGBT",
                         "portRX",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".portTX";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "LpGBT",
                         "portTX",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".elinkRX";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "LpGBT",
                         "elinkRX",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".elinkTX";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "LpGBT",
                         "elinkTX",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Path";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "LpGBT",
                         "Path",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".State";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "LpGBT",
                         "State",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Status";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "LpGBT",
                         "Status",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".VDAC";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "LpGBT",
                         "VDAC",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Temp";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "LpGBT",
                         "Temp",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".Reconfigure";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "LpGBT",
                         "Reconfigure",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".ConfigFile";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "LpGBT",
                         "ConfigFile",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".SecLpGBTPort";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "LpGBT",
                         "SecLpGBTPort",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".SecLpGBTConfigFile";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "LpGBT",
                         "SecLpGBTConfigFile",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".SecLpGBTI2C";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "LpGBT",
                         "SecLpGBTI2C",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }


        }
    }

    dyn_int children;

}


bool pp2AMACconfigureSubSystem (
    int     docNum,
    int     childNode,
    string  prefix,
    bool    createDps,
    bool    assignAddresses,
    bool    continueOnError,
    mapping addressActiveControl,
    mapping connectionSettings)
{
    DebugTN("Configure.SubSystem called");
    string name;
    xmlGetElementAttribute(docNum, childNode, "name", name);
    string fullName = prefix+name;
    string dpt = "PP2AMACSubSystem";

    if (pp2AMACdpTypeExists(dpt))
    {

        if (createDps)
        {

            DebugTN("Will create DP "+fullName);
            int result = dpCreate(fullName, dpt);
            if (result != 0)
            {
                DebugTN("dpCreate name='"+fullName+"' dpt='"+dpt+"' not successful or already existing");
                if (!continueOnError)
                    throw(makeError("Cacophony", PRIO_SEVERE, ERR_IMPL, 1, "XXX YYY ZZZ"));
            }
        }

        if (assignAddresses)
        {
            string dpe, address;
            dyn_string dsExceptionInfo;
            bool success;
            bool active = false;



        }
    }

    dyn_int children;
    children = pp2AMACgetChildNodesWithName(docNum, childNode, "FibreA");
    for (int i=1; i<=dynlen(children); i++)
        pp2AMACconfigureFibreA (docNum, children[i], fullName+"/", createDps, assignAddresses, continueOnError, addressActiveControl, connectionSettings);

}


bool pp2AMACconfigureFibreA (
    int     docNum,
    int     childNode,
    string  prefix,
    bool    createDps,
    bool    assignAddresses,
    bool    continueOnError,
    mapping addressActiveControl,
    mapping connectionSettings)
{
    DebugTN("Configure.FibreA called");
    string name;
    xmlGetElementAttribute(docNum, childNode, "name", name);
    string fullName = prefix+name;
    string dpt = "PP2AMACFibreA";

    if (pp2AMACdpTypeExists(dpt))
    {

        if (createDps)
        {

            DebugTN("Will create DP "+fullName);
            int result = dpCreate(fullName, dpt);
            if (result != 0)
            {
                DebugTN("dpCreate name='"+fullName+"' dpt='"+dpt+"' not successful or already existing");
                if (!continueOnError)
                    throw(makeError("Cacophony", PRIO_SEVERE, ERR_IMPL, 1, "XXX YYY ZZZ"));
            }
        }

        if (assignAddresses)
        {
            string dpe, address;
            dyn_string dsExceptionInfo;
            bool success;
            bool active = false;

            dpe = fullName+".RefreshPeriod_msec";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "FibreA",
                         "RefreshPeriod_msec",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".InfluxHostname";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "FibreA",
                         "InfluxHostname",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }


        }
    }

    dyn_int children;
    children = pp2AMACgetChildNodesWithName(docNum, childNode, "Bus");
    for (int i=1; i<=dynlen(children); i++)
        pp2AMACconfigureBus (docNum, children[i], fullName+"/", createDps, assignAddresses, continueOnError, addressActiveControl, connectionSettings);
    children = pp2AMACgetChildNodesWithName(docNum, childNode, "LpGBT");
    for (int i=1; i<=dynlen(children); i++)
        pp2AMACconfigureLpGBT (docNum, children[i], fullName+"/", createDps, assignAddresses, continueOnError, addressActiveControl, connectionSettings);

}


bool pp2AMACconfigureBus (
    int     docNum,
    int     childNode,
    string  prefix,
    bool    createDps,
    bool    assignAddresses,
    bool    continueOnError,
    mapping addressActiveControl,
    mapping connectionSettings)
{
    DebugTN("Configure.Bus called");
    string name;
    xmlGetElementAttribute(docNum, childNode, "name", name);
    string fullName = prefix+name;
    string dpt = "PP2AMACBus";

    if (pp2AMACdpTypeExists(dpt))
    {

        if (createDps)
        {

            DebugTN("Will create DP "+fullName);
            int result = dpCreate(fullName, dpt);
            if (result != 0)
            {
                DebugTN("dpCreate name='"+fullName+"' dpt='"+dpt+"' not successful or already existing");
                if (!continueOnError)
                    throw(makeError("Cacophony", PRIO_SEVERE, ERR_IMPL, 1, "XXX YYY ZZZ"));
            }
        }

        if (assignAddresses)
        {
            string dpe, address;
            dyn_string dsExceptionInfo;
            bool success;
            bool active = false;

            dpe = fullName+".Path";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "Bus",
                         "Path",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".CalibrationsDir";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "Bus",
                         "CalibrationsDir",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".AutoLoadCalibration";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "Bus",
                         "AutoLoadCalibration",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".AutoCalibrate";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "Bus",
                         "AutoCalibrate",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_INPUT_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".BusIdentifier";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "Bus",
                         "BusIdentifier",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".UploadToInflux";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "Bus",
                         "UploadToInflux",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".WriteConfig";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "Bus",
                         "WriteConfig",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".DCDCOn";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "Bus",
                         "DCDCOn",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".HVOn";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "Bus",
                         "HVOn",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".ShuntEn";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "Bus",
                         "ShuntEn",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".DACShunty";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "Bus",
                         "DACShunty",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".DACShuntx";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "Bus",
                         "DACShuntx",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".setIDs_bond";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "Bus",
                         "setIDs_bond",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".setIDs_fuse";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "Bus",
                         "setIDs_fuse",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".BusEnabledRegisters";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "Bus",
                         "BusEnabledRegisters",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".MonitorFullBus";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "Bus",
                         "MonitorFullBus",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }
            dpe = fullName+".LogDebug";
            address = dpe; // address can be generated from dpe after some mods ...
            strreplace(address, "/", ".");

            active = pp2AMACevaluateActive(
                         addressActiveControl,
                         "Bus",
                         "LogDebug",
                         dpe);

            success = pp2AMACaddressConfigWrapper(
                          dpe,
                          address,
                          DPATTR_ADDR_MODE_IO_SPONT /* mode */,
                          connectionSettings,
                          active);

            if (!success)
            {
                DebugTN("Failed setting address "+address+"; will terminate now.");
                return false;
            }


        }
    }

    dyn_int children;
    children = pp2AMACgetChildNodesWithName(docNum, childNode, "AMAC");
    for (int i=1; i<=dynlen(children); i++)
        pp2AMACconfigureAMAC (docNum, children[i], fullName+"/", createDps, assignAddresses, continueOnError, addressActiveControl, connectionSettings);

}


dyn_int pp2AMACgetChildNodesWithName (int docNum, int parentNode, string name)
{
    dyn_int result;
    int node = xmlFirstChild(docNum, parentNode);
    while (node >= 0)
    {
        if (xmlNodeName(docNum, node)==name)
            dynAppend(result, node);
        node = xmlNextSibling (docNum, node);
    }
    return result;
}

int pp2AMACparseConfig (
    string  configFileName,
    bool    createDps,
    bool    assignAddresses,
    bool    continueOnError,
    mapping addressActiveControl = makeMapping(),
    mapping connectionSettings = makeMapping())
/* Create instances */
{

    /* Apply defaults in connectionSettings, when not concretized by the user */
    if (!mappingHasKey(connectionSettings, CONNECTIONSETTING_KEY_DRIVER_NUMBER))
    {
        connectionSettings[CONNECTIONSETTING_KEY_DRIVER_NUMBER] = 120;
    }
    if (!mappingHasKey(connectionSettings, CONNECTIONSETTING_KEY_SERVER_NAME))
    {
        connectionSettings[CONNECTIONSETTING_KEY_SERVER_NAME] = "PP2_AMAC";
    }
    if (!mappingHasKey(connectionSettings, CONNECTIONSETTING_KEY_SUBSCRIPTION_NAME))
    {
        connectionSettings[CONNECTIONSETTING_KEY_SUBSCRIPTION_NAME] = "PP2_AMAC_SUBSCRIPTION";
    }

    /* Pre/Suffix the expression with ^$ to enable exact matches and also check if given patterns make sense */
    for (int i=1; i<=mappinglen(addressActiveControl); i++)
    {
        string regexp = mappingGetValue(addressActiveControl, i);
        regexp = "^"+regexp+"$";
        addressActiveControl[mappingGetKey(addressActiveControl, i)] = regexp;
        int regexpResult = regexpIndex(regexp, "thisdoesntmatter");
        if (regexpResult <= -2)
        {
            DebugTN("It seems that the given regular expression is wrong: "+regexp+"    the process will be aborted");
            return -1;
        }
    }

    string errMsg;
    int errLine;
    int errColumn;

    string configFileToLoad = configFileName;

    if (! _UNIX)
    {
        DebugTN("This code was validated only on Linux systems. For Windows, BE-ICS should perform the validation and release the component. See at https://its.cern.ch/jira/browse/OPCUA-1519 for more information.");
        return -1;
    }

    // try to perform entity substitution
    string tempFile = configFileToLoad + ".temp";
    int result = system("xmllint --noent " + configFileToLoad + " > " + tempFile);
    DebugTN("The call to 'xmllint --noent' resulted in: "+result);
    if (result != 0)
    {
        DebugTN("It was impossible to run xmllint to inflate entities. WinCC OA might load this file incorrectly if entity references are used. So we decided it wont be possible. See at https://its.cern.ch/jira/browse/OPCUA-1519 for more information.");
        return -1;
    }
    configFileToLoad = tempFile;

    int docNum = xmlDocumentFromFile(configFileToLoad, errMsg, errLine, errColumn);
    if (docNum < 0)
    {
        DebugN("Didn't open the file: at Line="+errLine+" Column="+errColumn+" Message=" + errMsg);
        return -1;
    }

    int firstNode = xmlFirstChild(docNum);
    if (firstNode < 0)
    {
        DebugN("Cant get the first child of the config file.");
        return -1;
    }

    while (xmlNodeName(docNum, firstNode) != "configuration")
    {
        firstNode = xmlNextSibling(docNum, firstNode);
        if (firstNode < 0)
        {
            DebugTN("configuration node not found, sorry.");
            return -1;
        }
    }

    // now firstNode holds configuration node
    dyn_int children;
    dyn_int children = pp2AMACgetChildNodesWithName(docNum, firstNode, "SubSystem");
    for (int i = 1; i<=dynlen(children); i++)
    {
        pp2AMACconfigureSubSystem (docNum, children[i], "", createDps, assignAddresses, continueOnError, addressActiveControl, connectionSettings);
    }


    return 0;
}
