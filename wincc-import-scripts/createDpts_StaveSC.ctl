

// generated using Cacophony, an optional module of quasar, see: https://github.com/quasar-team/Cacophony


//DCDCen
bool staveSCcreateDptDCDCen()
{
    // the names of vars and the way of generating DPT come directly from examples of dpTypeCreate
    dyn_dyn_string xxdepes;
    dyn_dyn_int xxdepei;
    dynAppend(xxdepes, makeDynString("StaveSCDCDCen", ""));
    dynAppend(xxdepei, makeDynInt(DPEL_STRUCT));
    dynAppend(xxdepes, makeDynString("", "DCDCen"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));



    int status = dpTypeCreate(xxdepes, xxdepei);
    return status == 0;
}

//HVcnt
bool staveSCcreateDptHVcnt()
{
    // the names of vars and the way of generating DPT come directly from examples of dpTypeCreate
    dyn_dyn_string xxdepes;
    dyn_dyn_int xxdepei;
    dynAppend(xxdepes, makeDynString("StaveSCHVcnt", ""));
    dynAppend(xxdepei, makeDynInt(DPEL_STRUCT));
    dynAppend(xxdepes, makeDynString("", "CntSetHV0en"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "CntSetHV1en"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "CntSetHV2en"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "CntSetHV3en"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "CntSetHV0Freq"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "CntSetHV1Freq"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "CntSetHV2Freq"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "CntSetHV3Freq"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));



    int status = dpTypeCreate(xxdepes, xxdepei);
    return status == 0;
}

//LDOcnt
bool staveSCcreateDptLDOcnt()
{
    // the names of vars and the way of generating DPT come directly from examples of dpTypeCreate
    dyn_dyn_string xxdepes;
    dyn_dyn_int xxdepei;
    dynAppend(xxdepes, makeDynString("StaveSCLDOcnt", ""));
    dynAppend(xxdepei, makeDynInt(DPEL_STRUCT));
    dynAppend(xxdepes, makeDynString("", "CntSetHyLDO0en"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "CntSetHyLDO1en"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "CntSetHyLDO2en"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "CntSetHxLDO0en"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "CntSetHxLDO1en"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "CntSetHxLDO2en"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));



    int status = dpTypeCreate(xxdepes, xxdepei);
    return status == 0;
}

//AMAC
bool staveSCcreateDptAMAC()
{
    // the names of vars and the way of generating DPT come directly from examples of dpTypeCreate
    dyn_dyn_string xxdepes;
    dyn_dyn_int xxdepei;
    dynAppend(xxdepes, makeDynString("StaveSCAMAC", ""));
    dynAppend(xxdepei, makeDynInt(DPEL_STRUCT));
    dynAppend(xxdepes, makeDynString("", "AMACID"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "AMACVersion"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_STRING));
    dynAppend(xxdepes, makeDynString("", "Enabled"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "State"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_STRING));
    dynAppend(xxdepes, makeDynString("", "Status"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_STRING));
    dynAppend(xxdepes, makeDynString("", "EnabledRegisters"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_STRING));
    dynAppend(xxdepes, makeDynString("", "RegistersToEnable"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_STRING));
    dynAppend(xxdepes, makeDynString("", "RegistersToDisable"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_STRING));
    dynAppend(xxdepes, makeDynString("", "MonitorDCDC"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "read"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "write"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "useFuseID"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "WriteConfig"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "DCDCOn"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "HVOn"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "CntSetHV0en"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "CntSetHV1en"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "CntSetHV2en"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "CntSetHV3en"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "CntSetHV0Freq"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "CntSetHV1Freq"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "CntSetHV2Freq"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "CntSetHV3Freq"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "ShuntEn"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "DACShunty"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "DACShuntx"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "recalibrate"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "CalibrationFile"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_STRING));
    dynAppend(xxdepes, makeDynString("", "temperatureX"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_FLOAT));
    dynAppend(xxdepes, makeDynString("", "temperatureY"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_FLOAT));
    dynAppend(xxdepes, makeDynString("", "temperaturePB"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_FLOAT));
    dynAppend(xxdepes, makeDynString("", "temperaturePTAT"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_FLOAT));
    dynAppend(xxdepes, makeDynString("", "temperatureCTAT"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_FLOAT));
    dynAppend(xxdepes, makeDynString("", "getCur10V"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_FLOAT));
    dynAppend(xxdepes, makeDynString("", "getCur1V"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_FLOAT));
    dynAppend(xxdepes, makeDynString("", "current10V"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_FLOAT));
    dynAppend(xxdepes, makeDynString("", "current1V"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_FLOAT));
    dynAppend(xxdepes, makeDynString("", "PP2_I_out"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_FLOAT));
    dynAppend(xxdepes, makeDynString("", "PP2_V_sense"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_FLOAT));
    dynAppend(xxdepes, makeDynString("", "PP2_V_ret"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_FLOAT));
    dynAppend(xxdepes, makeDynString("", "PP2_V_in"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_FLOAT));
    dynAppend(xxdepes, makeDynString("", "PP2_V_out"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_FLOAT));
    dynAppend(xxdepes, makeDynString("", "DCDCen"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "DCDCenC"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "VDCDC"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "VDDLR"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "DCDCin"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "VDDREG"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "sysBG"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "AM900BG"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "AM600BG"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "CAL"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "AMref"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "CALx"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "CALy"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Shuntx"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Shunty"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "HGND"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "DIETEMP"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "NTCx"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "NTCy"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "NTCpb"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "VoltNTCx"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "VoltNTCy"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "VoltNTCpb"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Hrefx"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Hrefy"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Cur10V"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Cur10VTPlow"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Cur10VTPhigh"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Cur1V"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Cur1VTPlow"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Cur1VTPhigh"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "HVret"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "PTAT"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg000"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_LONG));
    dynAppend(xxdepes, makeDynString("", "Reg001"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg002"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg003"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg004"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg005"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg006"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg007"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg010"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_LONG));
    dynAppend(xxdepes, makeDynString("", "Reg010_Ch0"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg010_Ch1"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg010_Ch2"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg010_Ae"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg011"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_LONG));
    dynAppend(xxdepes, makeDynString("", "Reg011_Ch3"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg011_Ch4"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg011_Ch5"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg011_Ae"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg012"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_LONG));
    dynAppend(xxdepes, makeDynString("", "Reg012_Ch6"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg012_Ch7"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg012_Ch8"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg012_Ae"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg013"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_LONG));
    dynAppend(xxdepes, makeDynString("", "Reg013_Ch09"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg013_Ch10"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg013_Ch11"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg013_Ae"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg014"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_LONG));
    dynAppend(xxdepes, makeDynString("", "Reg014_Ch12"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg014_Ch13"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg014_Ch14"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg014_Ae"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg015"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_LONG));
    dynAppend(xxdepes, makeDynString("", "Reg015_Ch15"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg015_Ae"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg024"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg025"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg026"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg027"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg031"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg032"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg033"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg034"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg040_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg041_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg042_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg043_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg044_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg045_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg046_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg047_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg048_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg049_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg050_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg051_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg052_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg053_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg054_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg055_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg056_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg057_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg058_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg060_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg061_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg062_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg063_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg064_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg065_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg070_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg071_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg072_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg073_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg074_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg075_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg076_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg077_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg078_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg079_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg080_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg081_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg082_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg083_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg084_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg085_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg086_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg087_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg090_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg091_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg092_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg093_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg094_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg095_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg096_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg100_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg101_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg102_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg103_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg104_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg105_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg106_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg107_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg108_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg109_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg110_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg111_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg112_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg113_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg114_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg115_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg116_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg117_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg118_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg119_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg120_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg121_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg122_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg123_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg124_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg125_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg126_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg127_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg128_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg129_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg130_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg131_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg132_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg133_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg134_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg135_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg136_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg137_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg138_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg139_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg140_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg141_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg142_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg143_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg144_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg145_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg146_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg147_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg148_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg149_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg150_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg151_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg152_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg153_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg154_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg155_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg156_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg157_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg158_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg159_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg160_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg161_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg162_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg163_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg164_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg165_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg166_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg167_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg168_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg169_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg170_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reg171_RW"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));



    int status = dpTypeCreate(xxdepes, xxdepei);
    return status == 0;
}

//LpGBT
bool staveSCcreateDptLpGBT()
{
    // the names of vars and the way of generating DPT come directly from examples of dpTypeCreate
    dyn_dyn_string xxdepes;
    dyn_dyn_int xxdepei;
    dynAppend(xxdepes, makeDynString("StaveSCLpGBT", ""));
    dynAppend(xxdepei, makeDynInt(DPEL_STRUCT));
    dynAppend(xxdepes, makeDynString("", "hostname"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_STRING));
    dynAppend(xxdepes, makeDynString("", "portRX"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "portTX"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "elinkRX"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "elinkTX"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Path"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_STRING));
    dynAppend(xxdepes, makeDynString("", "State"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_STRING));
    dynAppend(xxdepes, makeDynString("", "Status"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_STRING));
    dynAppend(xxdepes, makeDynString("", "VDAC"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Temp"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "Reconfigure"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "ConfigFile"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_STRING));
    dynAppend(xxdepes, makeDynString("", "SecLpGBTPort"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "SecLpGBTConfigFile"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_STRING));
    dynAppend(xxdepes, makeDynString("", "SecLpGBTI2C"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));



    int status = dpTypeCreate(xxdepes, xxdepei);
    return status == 0;
}

//SubSystem
bool staveSCcreateDptSubSystem()
{
    // the names of vars and the way of generating DPT come directly from examples of dpTypeCreate
    dyn_dyn_string xxdepes;
    dyn_dyn_int xxdepei;
    dynAppend(xxdepes, makeDynString("StaveSCSubSystem", ""));
    dynAppend(xxdepei, makeDynInt(DPEL_STRUCT));



    int status = dpTypeCreate(xxdepes, xxdepei);
    return status == 0;
}

//FibreA
bool staveSCcreateDptFibreA()
{
    // the names of vars and the way of generating DPT come directly from examples of dpTypeCreate
    dyn_dyn_string xxdepes;
    dyn_dyn_int xxdepei;
    dynAppend(xxdepes, makeDynString("StaveSCFibreA", ""));
    dynAppend(xxdepei, makeDynInt(DPEL_STRUCT));
    dynAppend(xxdepes, makeDynString("", "Enabled"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "RefreshPeriod_msec"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_FLOAT));
    dynAppend(xxdepes, makeDynString("", "InfluxHostname"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_STRING));



    int status = dpTypeCreate(xxdepes, xxdepei);
    return status == 0;
}

//Bus
bool staveSCcreateDptBus()
{
    // the names of vars and the way of generating DPT come directly from examples of dpTypeCreate
    dyn_dyn_string xxdepes;
    dyn_dyn_int xxdepei;
    dynAppend(xxdepes, makeDynString("StaveSCBus", ""));
    dynAppend(xxdepei, makeDynInt(DPEL_STRUCT));
    dynAppend(xxdepes, makeDynString("", "Path"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_STRING));
    dynAppend(xxdepes, makeDynString("", "CalibrationsDir"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_STRING));
    dynAppend(xxdepes, makeDynString("", "AutoLoadCalibration"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "AutoCalibrate"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "BusIdentifier"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "UploadToInflux"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "WriteConfig"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "DCDCOn"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "HVOn"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "ShuntEn"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "DACShunty"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "DACShuntx"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_INT));
    dynAppend(xxdepes, makeDynString("", "setIDs_bond"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "setIDs_fuse"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "BusEnabledRegisters"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_STRING));
    dynAppend(xxdepes, makeDynString("", "MonitorFullBus"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));
    dynAppend(xxdepes, makeDynString("", "LogDebug"));
    dynAppend(xxdepei, makeDynInt(0, DPEL_BOOL));



    int status = dpTypeCreate(xxdepes, xxdepei);
    return status == 0;
}

int staveSCcreateDpts (string dptFilter=".*")
{
    {
        int result = regexpIndex(dptFilter, "DCDCen");
        if (result >= 0)
        {
            if (!staveSCcreateDptDCDCen())
                return 1;
        }
        else
        {
            DebugN("DPT DCDCen not covered by provided dptFilter, skipping");
        }
    }
    {
        int result = regexpIndex(dptFilter, "HVcnt");
        if (result >= 0)
        {
            if (!staveSCcreateDptHVcnt())
                return 1;
        }
        else
        {
            DebugN("DPT HVcnt not covered by provided dptFilter, skipping");
        }
    }
    {
        int result = regexpIndex(dptFilter, "LDOcnt");
        if (result >= 0)
        {
            if (!staveSCcreateDptLDOcnt())
                return 1;
        }
        else
        {
            DebugN("DPT LDOcnt not covered by provided dptFilter, skipping");
        }
    }
    {
        int result = regexpIndex(dptFilter, "AMAC");
        if (result >= 0)
        {
            if (!staveSCcreateDptAMAC())
                return 1;
        }
        else
        {
            DebugN("DPT AMAC not covered by provided dptFilter, skipping");
        }
    }
    {
        int result = regexpIndex(dptFilter, "LpGBT");
        if (result >= 0)
        {
            if (!staveSCcreateDptLpGBT())
                return 1;
        }
        else
        {
            DebugN("DPT LpGBT not covered by provided dptFilter, skipping");
        }
    }
    {
        int result = regexpIndex(dptFilter, "SubSystem");
        if (result >= 0)
        {
            if (!staveSCcreateDptSubSystem())
                return 1;
        }
        else
        {
            DebugN("DPT SubSystem not covered by provided dptFilter, skipping");
        }
    }
    {
        int result = regexpIndex(dptFilter, "FibreA");
        if (result >= 0)
        {
            if (!staveSCcreateDptFibreA())
                return 1;
        }
        else
        {
            DebugN("DPT FibreA not covered by provided dptFilter, skipping");
        }
    }
    {
        int result = regexpIndex(dptFilter, "Bus");
        if (result >= 0)
        {
            if (!staveSCcreateDptBus())
                return 1;
        }
        else
        {
            DebugN("DPT Bus not covered by provided dptFilter, skipping");
        }
    }
    return 0;
}